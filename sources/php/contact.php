<?php

$field_first_name = $_POST['firstName'];
$field_last_name = $_POST['lastName'];

$field_email = $_POST['email'];

$field_subjects = $_POST['subjects'];

$field_message = $_POST['message'];

$mail_to = 'info@speedtomarket.io';

$subject = 'Message from a contact form '.$field_subjects;

$body_message = 'From: '.$field_first_name." $field_last_name"."\n";

$body_message .= 'E-mail: '.$field_email."\n";

$body_message .= 'Message: '.$field_message;

$headers = 'From: '.$field_email."\r\n";

$headers .= 'Reply-To: '.$field_email."\r\n";

$mail_status = mail($mail_to, $subject, $body_message, $headers);


if ($mail_status) { ?>
	<script language="javascript" type="text/javascript">
		//alert('Thank you for the message. We will contact you shortly.');
		window.location = '../index.html';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		//alert('Message failed. Please, send an email to info@speedtomarket.io');
		window.location = '../index.html';
	</script>
<?php
}
?>