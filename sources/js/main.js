$(document).ready(function () {
    // handle hamburger button
    function removeClassSideNav() {
        $(".js-sidenav-overlay").removeClass("open-menu");
        $(".js-hamburger-menu").removeClass("show-content");
    }

    $(".js-hamburger-btn").click(function () {
        $(".js-hamburger-menu").toggleClass("show-content");
        $('.js-sidenav-overlay').toggleClass("open-menu");
    });

    $(".js-sidenav-overlay").click(function () {
        removeClassSideNav();
    });

    $(".js-overview").click(function (e) {
        e.preventDefault();
        $(".js-content-collapse").toggleClass("show-sub-list-menu");
    });

    // Auto scroll when click neo element
    $("a[href]").on('click', function (e) {
        var targetEle = this.hash;
        var $targetEle = $(targetEle);
        if ($targetEle && Object.keys($targetEle).length) {
            if ($(".js-sidenav-overlay").hasClass("open-menu")) {
                removeClassSideNav();
            }

            $("html, body").stop().animate({
                "scrollTop": $targetEle.offset().top
            }, 800, "swing", function() {
                window.location.hash = targetEle;
            });
        }
    });
});

window.addEventListener('load', () => {
    if (window.innerWidth > 992) {
        $(".js-handle-section").each(function () {
            var expectHeight = $(this).find(".stm-section__media").outerHeight() * 0.75;
            var curHeight = $(this).find(".card-primary").outerHeight();
            if (curHeight < expectHeight) {
                $(this).find(".card-primary").outerHeight(expectHeight);
            }
        });
    }
    
    if(window.location.hash && $(window.location.hash)) {
        $("html, body").stop().animate({
            "scrollTop": $(window.location.hash).offset().top
        }, 800, "swing");
    }
});

$(window).resize(function () {
    if (window.innerWidth > 992) {
        $(".js-handle-section").each(function () {
            var expectHeight = $(this).find(".stm-section__media").outerHeight() * 0.75;
            var curHeight = $(this).find(".card-primary").outerHeight();
            if (curHeight < expectHeight) {
                $(this).find(".card-primary").outerHeight(expectHeight);
            }
        });
    }
});