function scrollTop(inputID) {
    $("html, body").stop().animate({
        "scrollTop": $(inputID).offset().top
    }, 300, "swing");
}

function checkFirstInputError(inputErr, inputID) {
    return inputErr ? inputErr : inputID
}

function addErrTextEmpty(inputID) {
    var name = $(inputID).attr("data-name");
    if (inputID === "#phone") {
        $(inputID).parent().parent().addClass("field-error")
        $(inputID).parent().parent().find(".js-txt-error").text("Please enter your " + name + ".").addClass("error");
    } else {
        $(inputID).parent().addClass("field-error")
        $(inputID).parent().find(".js-txt-error").text("Please enter your " + name + ".").addClass("error");
    }
}

function addErrTextVaidate(inputID, txtErr) {
    if (inputID === "#phone") {
        $(inputID).parent().parent().addClass("field-error")
        $(inputID).parent().parent().find(".js-txt-error").text(txtErr).addClass("error");
    } else {
        $(inputID).parent().addClass("field-error")
        $(inputID).parent().find(".js-txt-error").text(txtErr).addClass("error");
    }
}

function removeErrText(inputID) {
    if (inputID === "#phone") {
        $(inputID).parent().parent().removeClass("field-error");
        $(inputID).parent().parent().find(".js-txt-error").text("").removeClass("error");
    } else {
        $(inputID).parent().removeClass("field-error");
        $(inputID).parent().find(".js-txt-error").text("").removeClass("error");
    }
}

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

function checkFirstName(firstNameID) {
    $(firstNameID) && $(firstNameID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(firstNameID);
        } else {
            removeErrText(firstNameID);
        }
    });
}

function checkLastName(lastNameID) {
    $(lastNameID) && $(lastNameID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(lastNameID);
        } else {
            removeErrText(lastNameID);
        }
    });
}

function checkEmai(emailID) {
    $(emailID) && $(emailID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(emailID);
        } else if (!validateEmail($(this).val())) {
            var txtErr = "Please enter a valid email address."
            addErrTextVaidate(emailID, txtErr);
        } else {
            removeErrText(emailID);
        }
    });
}

function checkCompany(companyID) {
    $(companyID) && $(companyID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(companyID);
        } else {
            removeErrText(companyID);
        }
    });
}

function checkPosition(positionID) {
    $(positionID) && $(positionID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(positionID);
        } else {
            removeErrText(positionID);
        }
    });
}

function checkCompanySize(companySizeID) {
    $(companySizeID) && $(companySizeID).change(function () {
        if ($(this).val() === "" || $(this).val() === null) {
            addErrTextEmpty(companySizeID);
        } else {
            removeErrText(companySizeID);
        }
    });
}

function checkCountry(countryID) {
    $(countryID) && $(countryID).change(function () {
        if ($(this).val() === "" || $(this).val() === null) {
            addErrTextEmpty(countryID);
        } else {
            removeErrText(countryID);
        }
    });
}

function checkPhone(phoneID) {
    $(phoneID) && $(phoneID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(phoneID);
        } else {
            removeErrText(phoneID);
        }
    });
}

function checkPromoCode(promoCodeID) {
    $(promoCodeID) && $(promoCodeID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(promoCodeID);
        } else {
            removeErrText(promoCodeID);
        }
    });
}

function checkSTMPackage(stmPackageID) {
    $(stmPackageID) && $(stmPackageID).change(function () {
        if ($(this).val() === "" || $(this).val() === null) {
            addErrTextEmpty(stmPackageID);
        } else {
            removeErrText(stmPackageID);
        }
    });
}

function checkStoreName(storeNameID) {
    $(storeNameID) && $(storeNameID).keyup(function () {
        if ($(this).val() === "") {
            addErrTextEmpty(storeNameID);
        } else {
            removeErrText(storeNameID);
        }
    });
}

 //validate contact us
 function checkSubjects(subjectsID) {
    $(subjectsID) && $(subjectsID).change(function () {
        if ($(this).val() === "" || $(this).val() === null) {
            addErrTextEmpty(subjectsID);
        } else {
            removeErrText(subjectsID);
        }
    });
}

$(document).ready(function () {
    // initialization national telephone
    var phoneIntlTelInput = document.querySelector("#phone");
    var countrySelect = document.querySelector("#country");
    if (phoneIntlTelInput) {
        var iti = intlTelInput(phoneIntlTelInput, {
            utilsScript: "bower/intl-tel-input/build/js/utils.js",
            autoPlaceholder: "off",
            separateDialCode: true
        });

        var countryData = window.intlTelInputGlobals.getCountryData();

        for (var i = 0; i < countryData.length; i++) {
            var country = countryData[i];
            var optionNode = document.createElement("option");
            optionNode.value = country.iso2;
            var textNode = document.createTextNode(country.name);
            optionNode.appendChild(textNode);
            countrySelect.appendChild(optionNode);
        }

        phoneIntlTelInput.addEventListener('countrychange', function (e) {
            countrySelect.value = iti.getSelectedCountryData().iso2;
        });

        countrySelect.addEventListener('change', function () {
            iti.setCountry(this.value);
        });
    }

    var firstName = "#firstName";
    var lastName = "#lastName";
    var email = "#email";
    var company = "#company";
    var position = "#position";
    var companySize = "#companySize";
    var country = "#country";
    var phone = "#phone";
    var promoCode = "#promoCode";
    var stmPackage = "#stmPackage";
    var storeName = "#storeName";

    //validate contact us
    var subjects = "#subjects";
    var message = "#message";

    var inputErr;
    var isCheckValidateRealTime = true;

    $("#submitForm").submit(function (e) {
        inputErr = "";

        if ($(firstName)) {
            if ($(firstName).val() === "") {
                addErrTextEmpty(firstName);
                inputErr = firstName;
            }
        }

        if ($(lastName)) {
            if ($(lastName).val() === "") {
                addErrTextEmpty(lastName);
                inputErr = checkFirstInputError(inputErr, lastName);
            }
        }

        if ($(email)) {
            if ($(email).val() === "") {
                addErrTextEmpty(email);
                inputErr = checkFirstInputError(inputErr, email);
            } else if (!validateEmail($(email).val())) {
                var txtErr = "Please enter a valid email address."
                addErrTextVaidate(email, txtErr);
                inputErr = checkFirstInputError(inputErr, email);
            }
        }

        if ($(company)) {
            if ($(company).val() === "") {
                addErrTextEmpty(company);
                inputErr = checkFirstInputError(inputErr, company);
            }
        }

        if ($(position)) {
            if ($(position).val() === "") {
                addErrTextEmpty(position);
                inputErr = checkFirstInputError(inputErr, position);
            }
        }

        if ($(companySize)) {
            if ($(companySize).val() === "" || $(companySize).val() === null) {
                addErrTextEmpty(companySize);
                inputErr = checkFirstInputError(inputErr, companySize);
            }
        }

        if ($(country)) {
            if ($(country).val() === "" || $(country).val() === null) {
                addErrTextEmpty(country);
                inputErr = checkFirstInputError(inputErr, country);
            }
        }

        if ($(phone)) {
            if ($(phone).val() === "") {
                addErrTextEmpty(phone);
                inputErr = checkFirstInputError(inputErr, phone);
            }
        }

        if ($(promoCode)) {
            if ($(promoCode).val() === "") {
                addErrTextEmpty(promoCode);
                inputErr = checkFirstInputError(inputErr, promoCode);
            }
        }

        if ($(stmPackage)) {
            if ($(stmPackage).val() === "" || $(stmPackage).val() === null) {
                addErrTextEmpty(stmPackage);
                inputErr = checkFirstInputError(inputErr, stmPackage);
            }
        }

        if ($(storeName)) {
            if ($(storeName).val() === "") {
                addErrTextEmpty(storeName);
                inputErr = checkFirstInputError(inputErr, storeName);
            }
        }

        //validate contact us
        if ($(subjects)) {
            if ($(subjects).val() === "" || $(subjects).val() === null) {
                addErrTextEmpty(subjects);
                inputErr = checkFirstInputError(inputErr, subjects);
            }
        }

        if (isCheckValidateRealTime) {
            checkFirstName(firstName);
            checkLastName(lastName);
            checkEmai(email);
            checkCompany(company);
            checkPosition(position);
            checkCompanySize(companySize);
            checkCountry(country);
            checkPhone(phone);
            checkSTMPackage(stmPackage);
            checkPromoCode(promoCode);
            checkStoreName(storeName);

            //validate contact us
            checkSubjects(subjects);
        }
        isCheckValidateRealTime = false;

        if (inputErr) {
            e.preventDefault();
            scrollTop(inputErr)
        } else {
            $("#submitBtn").attr("disabled", "disabled");
            setTimeout(() => {
                $("#submitBtn").removeAttr("disabled");
            }, 5000);
            // handle api
        }
    });
});