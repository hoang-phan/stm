module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            less: {
                files: ['sources/less/**/**.less'],
                tasks: ['less'],
            },
            nunjucks: {
                files: ['sources/page/**/**.html'],
                tasks: ['nunjucks']
            }
        },

        less: {
            default: {
                options: {
                    compress: false,
                    sourceMap: true
                },
                files: [
                   {
                        'sources/css/stm.css': 'sources/less/stm.less'
                    }
                ]
            }
        },
        nunjucks: {
            options: {
                noCache: true,
                data: 'sources/templates/data.json'
            },
            render: {
                files: [{
                    expand: true,
                    cwd: 'sources/page/',
                    src: ['*.html'],
                    dest: 'sources/'
                }]
            }
        }

    });

    // Plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-nunjucks-2-html');

    // Default task(s).
    grunt.registerTask('default', ['watch']);


};